# Frontend Docker Build Instructions

This document explains the steps to build and run a frontend application using a multi-stage Dockerfile.

## Multi-Stage Dockerfile

### Stage 1: Base Stage

```dockerfile
# Instruction to build my frontend. (multi-stage build)

FROM node:14-alpine as base

WORKDIR /frontend

COPY /todo-list-todoapp/package*.json /frontend

RUN npm install

COPY /todo-list-todoapp /frontend

RUN npm run build
