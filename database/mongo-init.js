db = db.getSiblingDB('todolist');

db.createUser({
  user: 'todoadmin',
  pwd: 'access',
  roles: [
    { role: 'readWrite', db: 'todolist' },
    { role: "root", db: "admin" }
  ]
});
