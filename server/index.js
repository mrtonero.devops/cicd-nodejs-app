const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const cors = require('cors');

const app = express();

// use express.json() to get data into json format
app.use(express.json());

//Port
const PORT = process.env.PORT || 5500;

//use cors
app.use(cors())

//Lets import Routes
const TodoItemRoute = require('./routes/todoItems')

const mongoURL = process.env.MONGO_URL;
console.log("mongodb url:", mongoURL)


//lets connect to mongodb
mongoose
  .connect(process.env.MONGO_URL || 'mongodb://localhost:27017/todolistdb',{
    useNewUrlParser: true,
    useUnifiedTopology: true,

  })
  .then(() => console.log("Database Connected"))
  .catch((err) => console.error("Error connecting to database:", err));


app.use('/', TodoItemRoute);



//Add Port and connect to server
app.listen(PORT, () => console.log(`Server connected on ${PORT}`));
