const request = require("supertest");
const express = require("express");
const mongoose = require("mongoose");
const todoItemsModel = require("../../models/todoItems");
const router = require("../../routes/todoItems");
const dotenv = require("dotenv").config();

// Create an Express application
const app = express();
app.use(express.json());
app.use("/", router);

// Increase the default timeout for Jest hooks and tests
jest.setTimeout(30000); // 30 seconds

// Connect to a test database
beforeAll(async () => {
  const dbURI = process.env.MONGO_URL; // Ensure this is set in your .env file
  await mongoose.connect(dbURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
});

// Clean the database before each test
beforeEach(async () => {
  await todoItemsModel.deleteMany({});
});

// Define a test case for the "POST /api/item" route (create item)
describe("POST /api/item", () => {
  it("should add a new Todo item", async () => {
    // Define a sample Todo item
    const item = { item: "Test Todo Item 1" };

    // Send a POST request to the route
    const response = await request(app)
      .post("/api/item")
      .send(item)
      .expect(200);

    // Check if the response contains the newly created item
    expect(response.body).toHaveProperty("_id");
    expect(response.body.item).toBe(item.item);

    // Optional: You can also check if the item exists in the database
    const savedItem = await todoItemsModel.findById(response.body._id);
    expect(savedItem).toBeTruthy();
    expect(savedItem.item).toBe(item.item);
  });
});

// Define a test case for the "DELETE /api/item/:id" route (delete item)
describe("DELETE /api/item/:id", () => {
  it("should delete a Todo item", async () => {
    // Insert a sample Todo item into the database for testing
    const newItem = await todoItemsModel.create({ item: "Todo Item to Delete" });

    // Send a DELETE request to delete the item
    const response = await request(app)
      .delete(`/api/item/${newItem._id}`)
      .expect(200);

    // Check if the response contains the "Item Deleted" message
    expect(response.body).toBe("Item Deleted");

    // Check if the item has been deleted from the database
    const deletedItem = await todoItemsModel.findById(newItem._id);
    expect(deletedItem).toBeNull();
  });

  it("should return an error for invalid item ID", async () => {
    // Send a DELETE request with an invalid item ID
    const invalidItemId = "64a73b22380d893bf8b04f66"; // Replace with a non-existent ID
    const response = await request(app)
      .delete(`/api/item/${invalidItemId}`)
      .expect(404);

    // Check if the response contains an error message
    expect(response.body).toHaveProperty("error");
  });
});

// Define a test case for the "PUT /api/item/:id" route (update item)
describe("PUT /api/item/:id", () => {
  it("should update a Todo item", async () => {
    // Insert a sample Todo item into the database for testing
    const newItem = await todoItemsModel.create({ item: "Original Todo Item" });

    // Define the updated item data
    const updatedItem = { item: "Updated Todo Item" };

    // Send a PUT request to update the item
    const response = await request(app)
      .put(`/api/item/${newItem._id}`)
      .send(updatedItem)
      .expect(200);

    // Check if the response contains the "Item Updated" message
    expect(response.body).toBe("Item Updated");

    // Check if the item has been updated in the database
    const retrievedItem = await todoItemsModel.findById(newItem._id);
    expect(retrievedItem.item).toBe(updatedItem.item);
  });
});

// Define a test case for the "GET /api/items" route (retrieve all items)
describe("GET /api/items", () => {
  it("should retrieve all Todo items", async () => {
    // Insert some sample Todo items into the database for testing
    await todoItemsModel.create([
      { item: "Test Todo Item 1" },
      { item: "Test Todo Item 2" },
    ]);

    // Send a GET request to the route
    const response = await request(app)
      .get("/api/items")
      .expect(200);

    // Check if the response is an array
    expect(Array.isArray(response.body)).toBe(true);

    // Check if the response contains the expected number of items
    expect(response.body.length).toBe(2);

    // Check if the response contains the expected items without assuming order
    const expectedItems = [
      { item: "Test Todo Item 1" },
      { item: "Test Todo Item 2" },
    ];
    expectedItems.forEach(expectedItem => {
      expect(response.body).toContainEqual(expect.objectContaining(expectedItem));
    });

    // Optionally, you can add more specific checks for the retrieved items
    response.body.forEach(item => {
      expect(item).toHaveProperty("_id");
      expect(item).toHaveProperty("item", expect.any(String));
    });
  });
});

// Disconnect from the test database after tests are done
afterAll(async () => {
  await mongoose.connection.close();
});

// Run a basic test
describe("Express API Tests", () => {
  it("should run the tests", () => {
    expect(1).toBe(1);
  });
});
